require('dotenv').config();

module.exports = {
  /**
   * Returns the default Cico address.
   * @return {String} Default Cico address.
   */
  getDefaultCicoAddress: () => {
    if (!process.env.SENDER_ADDRESS) {
      throw Error('Must have SENDER_ADDRESS in .env');
    }
    return String(Buffer.from(process.env.SENDER_ADDRESS));
  },

  /**
   * Returns the Cico network RPC url.
   * @return {String} The Cico network RPC url.
   */
  getCicoRPCAddress: () => {
    if (!process.env.CICO_RPC_ADDRESS) {
      throw Error('Must have CICO_RPC_ADDRESS in .env');
    }
    return String(Buffer.from(process.env.CICO_RPC_ADDRESS));
  },

  /**
   * Returns the wallet passphrase to unlock the encrypted wallet.
   * @return {String} The wallet passphrase.
   */
  getWalletPassphrase: () => (process.env.WALLET_PASSPHRASE ? String(Buffer.from(process.env.WALLET_PASSPHRASE)) : ''),

  isWalletEncrypted: async (cweb3) => {
    const res = await cweb3.getWalletInfo();
    return Object.prototype.hasOwnProperty.call(res, 'unlocked_until');
  },
};
